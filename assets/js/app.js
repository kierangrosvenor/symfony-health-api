/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.scss in this case)
require('../css/app.scss');

const React = require('react');
const ReactDOM = require('react-dom');

import App from './app/app'

ReactDOM.render(
    <App/>,
    document.getElementById('app')
);

