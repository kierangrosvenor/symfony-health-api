import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Router,  Route, Switch } from 'react-router-dom';
import _ from 'lodash'

import PrivateRoute from "./components/PrivateRoute";
import DashboardPage from "./pages/DashboardPage";


import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import HomePage from "./pages/HomePage";

import {history} from "./app";
import SettingsPage from "./pages/SettingsPage";
import Header from './components/Header'
import GlucosePage from "./pages/GlucosePage";

class Routes extends Component {

    componentDidUpdate() {
       return false;
    }

    render() {

        let isAuthed = this.props.auth.token !== null;

        return (
            <Router history={history}>
                <div>
                    <Header/>
                    <Switch>
                        <Route exact  path="/" component={HomePage}/>
                        <Route exact path="/login" component={LoginPage} />
                        <Route exact path="/register" component={RegisterPage} />
                        <PrivateRoute exact path="/settings" component={SettingsPage} authed={isAuthed}  />
                        <PrivateRoute exact path="/dashboard" component={DashboardPage} authed={isAuthed}  />
                        <PrivateRoute exact path="/glucose" component={GlucosePage} authed={isAuthed}  />
                    </Switch>

                </div>
            </Router>
        );
    }
}

const mapStateToProps = state => ({ auth: state.auth });
export default connect(mapStateToProps)(Routes);

