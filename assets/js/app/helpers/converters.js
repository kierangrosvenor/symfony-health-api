//biometric conversion formulae

/**
 * mg/dL to mmol/L
 * @param mgdl
 * @returns {number}
 */
export const mgdlToMmoll = (mgdl) => {
    return (mgdl / 18);
};

/**
 * mmol/L to mg/dL
 * @param mmoll
 * @returns {number}
 */
export const mmolToMgdl = (mmoll) => {
    return (mmoll * 18);
};


/**
 * Pounds to kilograms
 * @param pounds
 * @returns {number}
 */
export const poundsToKilograms = (pounds) => {
   return (pounds / 2.2046)
};

/**
 * Kilograms to pounds
 * @param kilograms
 * @returns {number}
 */
export const kilogramsToPounds = (kilograms) => {
    return (kilograms * 2.2046);
};


/**
 * Kilograms to stones and pounds
 * @param kilograms
 * @returns {{pounds: number, stones: number}}
 */
export const kilogramsToStonesAndPounds = (kilograms) => {
    let stones = (kilograms / 6.35029318), pounds;
    let remainder = stones % 1;
    stones = Math.floor(stones) + remainder;
    pounds = Math.round((remainder * 14));
    return {
        stones: Math.floor(stones),
        pounds: Math.floor(pounds)
    }
};

/**
 * Stones and pounds to kilograms
 * @param stones
 * @param pounds
 * @returns {number}
 */
export const stonesAndPoundsToKilograms = (stones, pounds) => {
    let kgFromStones = (6.35029318 * stones);
    let kgFromPounds = (0.45359237 * pounds);
    return (kgFromStones + kgFromPounds);
};