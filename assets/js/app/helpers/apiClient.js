import axios from 'axios'

let apiClient = axios.create({
    baseURL: '/api/'
});

export default apiClient;
