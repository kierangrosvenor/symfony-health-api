import {combineReducers} from "redux"
import authReducer from './auth'
import dashboardReducer from './dashboard'
import weightReducer from './weight'
import glucoseReducer from './glucose'

export default combineReducers({
    auth: authReducer,
    dashboard: dashboardReducer,
    weight: weightReducer,
    glucose: glucoseReducer
});