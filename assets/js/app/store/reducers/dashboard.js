import {
    SET_BIOMETRICS_LATEST
} from "../types/dashboard";

const initialState = {
    latestBiometrics: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_BIOMETRICS_LATEST:
            return {
                ...state,
                latestBiometrics: action.payload
            };
        default:
            return state;
    }
}