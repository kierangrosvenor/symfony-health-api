import {
    SET_WEIGHT_TREND
} from "../types/weight";

const initialState = {
    trend: []
};

export default function(state = initialState, action) {
    switch(action.type) {
        case SET_WEIGHT_TREND:
            let data = action.payload, trend = [];
            data.forEach(function (item) {
                trend.push(item.kilograms);
            });
            return { 
                ...state,
                trend: trend
            };
        default:
            return state;
    }
}