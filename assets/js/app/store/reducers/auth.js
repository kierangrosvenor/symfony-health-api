import {
    SET_TOKEN,
    SET_AUTH_PENDING,
    SET_USER,
    SET_AUTH_CREDENTIAL_ERROR
} from "../types/auth";

const initialState = {
    isCredentialError: false,
    isFormSubmitting: false,
    token: null,
    user: {}
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_TOKEN:
            return {
                ...state,
                token: action.payload
            };
        case SET_USER:
            return {
                ...state,
                user: action.payload
            };
        case SET_AUTH_CREDENTIAL_ERROR:
            return {
                ...state,
                isCredentialError: action.payload
            };
        case SET_AUTH_PENDING:
            return {
                ...state,
                isFormSubmitting: action.payload
            };
        default:
            return state;
    }
}