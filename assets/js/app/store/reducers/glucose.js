import {SET_GLUCOSE_PAGINATION, SET_GLUCOSE_TREND} from "../types/glucose";

const initialState = {
    paginationInfo: {
        next: 1
    },
    data: [],
    trend: []
};

export default function (state = initialState, action) {
    switch (action.type) {



        case SET_GLUCOSE_PAGINATION:
            let payload = action.payload;
            let paginationInfo = payload.paginationInfo;

            return {
                ...state,
                paginationInfo: paginationInfo,
                data: state.data
                    .push
                    .apply(state.data, payload.data)
            }



        case SET_GLUCOSE_TREND:
            let data = action.payload, trend = [];
            data.forEach(function (item) {
                trend.push(item.mgdl);
            });
            return {
                ...state,
                trend: trend
            };

        default:
            return state;
    }
}