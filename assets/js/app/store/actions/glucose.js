import apiClient from '../../helpers/apiClient'
import moment from 'moment'
import {SET_GLUCOSE_TREND} from "../types/glucose";

export const getGlucoseTrend = () => dispatch => {
    apiClient.get("/biometric/glucose/between", {
        params: {
            page: 1,
            per_page: 250,
            from_date: moment().startOf('day').toISOString(),
            to_date: moment().endOf('day').toISOString()
        }
    })
        .then(function (response) {
            dispatch({
                type: SET_GLUCOSE_TREND,
                payload: response.data.data
            })
        })
};