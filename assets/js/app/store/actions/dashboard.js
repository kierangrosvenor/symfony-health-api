import {SET_BIOMETRICS_LATEST} from "../types/dashboard";
import apiClient from '../../helpers/apiClient'


export const setBiometricsLatest = (data) => dispatch => {
    dispatch({
        type: SET_BIOMETRICS_LATEST,
        payload: data
    })
};


export const getBiometricsLatest = () => dispatch => {
    apiClient.get("/biometrics/latest/")
        .then(function (response) {
            dispatch(setBiometricsLatest(response.data));
        })
};