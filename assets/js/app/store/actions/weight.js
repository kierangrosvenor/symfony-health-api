import moment from 'moment'
import apiClient from '../../helpers/apiClient'
import {SET_WEIGHT_TREND} from "../types/weight";

export const getWeightTrend = () => dispatch => {
    apiClient.get("/biometric/weight/between", {
        params: {
            page: 1,
            per_page: 250,
            from_date: moment().startOf('week').toISOString(),
            to_date: moment().endOf('week').toISOString()
        }
    })
        .then(function (response) {
            dispatch({
                type: SET_WEIGHT_TREND,
                payload: response.data.data
            })
        })
}