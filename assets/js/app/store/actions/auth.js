import apiClient from '../../helpers/apiClient'
import {SET_AUTH_CREDENTIAL_ERROR, SET_AUTH_PENDING, SET_TOKEN, SET_USER} from "../types/auth";
import {history} from "../../app";
import _ from 'lodash'
import Cookies from 'js-cookie'

export const setToken = (token) => dispatch => {

    Cookies.set('token', token);
    if(token === null) {
        Cookies.remove('token')
    }

    apiClient.defaults.headers.common = {
        'Authorization': 'Bearer ' + token
    };

    dispatch({
        type: SET_TOKEN,
        payload: token
    })
};

export const getUser = (isLogin) => dispatch => {


    if(isLogin) {
        dispatch({
            type: SET_AUTH_PENDING,
            payload: true
        })
    }

    apiClient.get("/user/")
        .then(function (response) {
            dispatch({
                type: SET_USER,
                payload: response.data
            });

            //if param is true, go to dashboard
            if (isLogin === true) {
                history.push("/dashboard")
            }
            dispatch({
                type: SET_AUTH_PENDING,
                payload: false
            })
        })
        .catch(function (error) {
            if (isLogin) {
                dispatch({
                    type: SET_AUTH_PENDING,
                    payload: false
                })
            }
            if(_.has(error, 'response')) {
                if(error.response.status === 401) {
                    history.push("/login");
                }
            }
        })
};

export const setUser = (user) => dispatch => {
    dispatch({
        type: SET_USER,
        payload: user
    })
};

export const logoutUser = () => dispatch => {
    Cookies.remove('token')

    dispatch({
        type: SET_TOKEN,
        payload: null
    });

    dispatch({
        type: SET_USER,
        payload: {}
    });

    history.push("/login");
};


export const loginUser = (values) => dispatch => {

    dispatch({
        type: SET_AUTH_PENDING,
        payload: true
    });

    apiClient.post("/auth/token/", values)
        .then(function (response) {
            dispatch(setToken(response.data.token));
            apiClient.defaults.headers.common = {
                'Authorization': 'Bearer ' + response.data.token
            };
            dispatch({
                type: SET_AUTH_PENDING,
                payload: false
            });
            dispatch(getUser(true));
        })
        .catch(function (err) {

            dispatch({
                type: SET_AUTH_PENDING,
                payload: false
            });

            if (_.has(err, 'response')) {
                console.log(err.response);
                if (err.response.status === 401) {

                    dispatch({
                        type: SET_AUTH_CREDENTIAL_ERROR,
                        payload: true
                    });

                    let timeout = setTimeout(function () {

                        dispatch({
                            type: SET_AUTH_CREDENTIAL_ERROR,
                            payload: false
                        });

                        clearTimeout(timeout);
                    }, 4000)
                }
            }
        })
};
