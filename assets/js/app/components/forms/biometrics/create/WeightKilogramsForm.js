import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Form, Button, Alert} from 'react-bootstrap'
import {Formik} from "formik";
import * as Yup from 'yup'
import {connect} from 'react-redux'
import apiClient from '../../../../helpers/apiClient'
import moment from 'moment'
import {getBiometricsLatest} from "../../../../store/actions/dashboard";
import {getWeightTrend} from "../../../../store/actions/weight";

class WeightKilogramsForm extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Formik
                initialValues={{
                    kilograms: undefined,
                    notes: ""
                }}
                onSubmit={(values, {setSubmitting, handleReset}) => {
                    let self = this;
                    let model = {
                        created_at: moment().toISOString(),
                        kilograms: values.kilograms,
                        notes: ""
                    };
                    apiClient.post("/biometric/weight/", model)
                        .then(function (response) {
                            console.log(response.data);
                            self.props.getBiometricsLatest();
                            self.props.getWeightTrend();
                            self.props.onSave();
                            setSubmitting(false);
                        })
                        .catch(function (error) {
                            console.log(error);
                            setSubmitting(false);
                        })

                }}
                validationSchema={Yup.object().shape({
                    kilograms: Yup.number()
                        .min(20)
                        .max(560)
                        .required('Required')
                })}
            >
                {props => {
                    const {
                        values,
                        touched,
                        errors,
                        //dirty,
                        isSubmitting,
                        handleChange,
                        //handleBlur,
                        handleSubmit,
                        //handleReset,
                    } = props;
                    return (
                        <Form noValidate onSubmit={handleSubmit}>
                            <Form.Group>
                                <Form.Label>Kilograms</Form.Label>
                                <Form.Control
                                    type="number"
                                    name="kilograms"
                                    onChange={handleChange}
                                    isValid={!errors.kilograms && touched.kilograms}
                                    isInvalid={!!errors.kilograms}
                                />
                            </Form.Group>
                            <Button
                                type="submit">
                                Save
                            </Button>
                        </Form>
                    );
                }}
            </Formik>
        )
    }
}

WeightKilogramsForm.propTypes = {
    onSave: PropTypes.func
};

const mapActionsToProps = {
    getBiometricsLatest,
    getWeightTrend
}

const mapStateToProps = (state) => {
   return {
       dashboard: state.dashboard
   };
};

export default connect(mapStateToProps, mapActionsToProps)(WeightKilogramsForm)