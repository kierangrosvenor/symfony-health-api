import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Form, Button, Alert} from 'react-bootstrap'
import {Formik} from "formik";
import * as Yup from 'yup'
import {connect} from 'react-redux'
import apiClient from '../../../../helpers/apiClient'
import moment from 'moment'
import {getBiometricsLatest} from "../../../../store/actions/dashboard";
import {getGlucoseTrend} from "../../../../store/actions/glucose";


class GlucoseMgdlForm extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        let modelDef = {
            mgdl: undefined,
            notes: "",
            meal_status: "BEFORE_MEAL"
        }

        return (
            <Formik
                initialValues={modelDef}
                onSubmit={(values, {setSubmitting, resetForm}) => {

                    let self = this;
                    let model = {
                        created_at: moment().toISOString(),
                        mgdl: values.mgdl,
                        meal_status: values.meal_status,
                        notes: ""
                    };

                    apiClient.post("/biometric/glucose/", model)
                        .then(function (response) {
                            console.log(response.data);
                            self.props.getBiometricsLatest();
                            self.props.getGlucoseTrend();
                            self.props.onSave(response.data);
                            setSubmitting(false);
                            resetForm(modelDef);
                        })
                        .catch(function (error) {
                            console.log(error);
                            setSubmitting(false);
                        })

                }}
                validationSchema={Yup.object().shape({
                mgdl: Yup.number()
                    .min(0)
                    .max(2657)
                    .required('Required'),
                meal_status: Yup.string()
                    .required('Required')
            })}

            >
                {props => {
                    const {
                        values,
                        touched,
                        errors,
                        //dirty,
                        isSubmitting,
                        handleChange,
                        //handleBlur,
                        handleSubmit,
                        resetForm,
                        handleReset
                    } = props;
                    return (
                        <Form noValidate onSubmit={handleSubmit}>
                            <Form.Group>
                                <Form.Label>mg/dL</Form.Label>
                                <Form.Control
                                    type="number"
                                    name="mgdl"
                                    value={values.mgdl || ''}
                                    onChange={handleChange}
                                    isValid={!errors.mgdl && touched.mgdl}
                                    isInvalid={!!errors.mgdl}
                                />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Before meal or after?</Form.Label>
                                <Form.Check
                                    type="radio"
                                    label="Before meal"
                                    name="gender"
                                    value="BEFORE_MEAL"
                                    id="select-meal-before"
                                    onChange={handleChange}
                                />
                                <Form.Check
                                    type="radio"
                                    label="After meal"
                                    name="gender"
                                    value="AFTER_MEAL"
                                    id="select-meal-after"
                                    onChange={handleChange}
                                />
                            </Form.Group>

                            <Button
                                type="submit">
                                Save
                            </Button>
                        </Form>
                    )
                }}
            </Formik>
        )
    }
}

GlucoseMgdlForm.propTypes = {
  onSave: PropTypes.func
};

// start of code change
const mapStateToProps = (state) => {
    return {
        auth: state.user,
        dashboard: state.dashboard
    };
};

export default connect(mapStateToProps, {getBiometricsLatest, getGlucoseTrend})(GlucoseMgdlForm)