import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Form, Button, Alert, Col, Row} from 'react-bootstrap'
import {Formik} from "formik";
import * as Yup from 'yup'
import {connect} from 'react-redux'
import apiClient from '../../../../helpers/apiClient'
import moment from 'moment'
import {getBiometricsLatest} from "../../../../store/actions/dashboard";
import {getWeightTrend} from "../../../../store/actions/weight";
import {stonesAndPoundsToKilograms} from "../../../../helpers/converters";


class WeightStonesPoundsForm extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Formik
                validationSchema={Yup.object().shape({
                    stones: Yup.number()
                        .min(1)
                        .max(50)
                        .required('Required'),
                    pounds: Yup.number()
                        .min(0)
                        .max(13)
                        .required('Required')
                })}
                onSubmit={(values, {setSubmitting}) => {
                    let self = this;
                    let model = {
                        created_at: moment().toISOString(),
                        kilograms: stonesAndPoundsToKilograms(values.stones, values.pounds),
                        notes: ""
                    };
                    apiClient.post("/biometric/weight/", model)
                        .then(function (response) {
                            console.log(response.data);
                            self.props.getBiometricsLatest();
                            self.props.getWeightTrend();
                            self.props.onSave();
                            setSubmitting(false);
                        })
                        .catch(function (error) {
                            console.log(error);
                            setSubmitting(false);
                        })

                }}

            >
                {props => {
                    const {
                        values,
                        touched,
                        errors,
                        //dirty,
                        isSubmitting,
                        handleChange,
                        //handleBlur,
                        handleSubmit,
                        //handleReset,
                    } = props;
                    return (
                        <Form noValidate onSubmit={handleSubmit}>

                            <Form.Row>

                                <Col sm="6" md="6" lg="6">
                                    <Form.Group>
                                        <Form.Label>Stones</Form.Label>
                                        <Form.Control
                                            type="number"
                                            name="stones"
                                            onChange={handleChange}
                                            isValid={!errors.stones && touched.stones}
                                            isInvalid={!!errors.stones}
                                        />
                                    </Form.Group>
                                </Col>
                                <Col sm="6" md="6" lg="6">
                                    <Form.Group>
                                        <Form.Label>Pounds</Form.Label>
                                        <Form.Control
                                            type="number"
                                            name="pounds"
                                            onChange={handleChange}
                                            isValid={!errors.pounds && touched.pounds}
                                            isInvalid={!!errors.pounds}
                                        />
                                    </Form.Group>
                                </Col>
                            </Form.Row>

                            <Button
                                disabled={isSubmitting}
                                type="submit">
                                Save
                            </Button>
                        </Form>
                    );
                }}
            </Formik>
        )
    }
}

WeightStonesPoundsForm.propTypes = {
    onSave: PropTypes.func
};

const mapActionsToProps = {
    getBiometricsLatest,
    getWeightTrend
}

const mapStateToProps = (state) => {
    return {
        dashboard: state.dashboard
    };
};

export default connect(mapStateToProps, mapActionsToProps)(WeightStonesPoundsForm)