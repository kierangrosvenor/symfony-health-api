import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Form, Button, Alert} from 'react-bootstrap'
import {Formik} from "formik";
import * as Yup from 'yup'
import {connect} from 'react-redux'
import apiClient from '../../../../helpers/apiClient'
import moment from 'moment'
import {getBiometricsLatest} from "../../../../store/actions/dashboard";
import {getWeightTrend} from "../../../../store/actions/weight";
import {poundsToKilograms} from "../../../../helpers/converters";


class WeightPoundsForm extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Formik
                onSubmit={(values, {setSubmitting}) => {
                    let self = this;
                    let kilograms = poundsToKilograms(values.pounds)
                    let model = {
                        created_at: moment().toISOString(),
                        kilograms: kilograms,
                        notes: ""
                    };
                    apiClient.post("/biometric/weight/", model)
                        .then(function (response) {
                            console.log(response.data);
                            self.props.getBiometricsLatest();
                            self.props.getWeightTrend();
                            self.props.onSave();
                            setSubmitting(false);
                        })
                        .catch(function (error) {
                            setSubmitting(false);
                        })
                }}
                validationSchema={Yup.object().shape({
                    pounds: Yup.number()
                        .min(0)
                        .max(1000)
                })}
            >
                {props => {
                    const {
                        values,
                        touched,
                        errors,
                        //dirty,
                        isSubmitting,
                        handleChange,
                        //handleBlur,
                        handleSubmit,
                        //handleReset,
                    } = props;
                    return (
                        <Form noValidate onSubmit={handleSubmit}>
                            <Form.Group>
                                <Form.Label>Pounds</Form.Label>
                                <Form.Control
                                    type="number"
                                    name="pounds"
                                    onChange={handleChange}
                                    isValid={!errors.pounds && touched.pounds}
                                    isInvalid={!!errors.pounds}
                                />
                            </Form.Group>
                            <Button
                                type="submit">
                                Save
                            </Button>
                        </Form>
                    );
                }}
            </Formik>
        )
    }
}

WeightPoundsForm.propTypes = {
    onSave: PropTypes.func
};

const mapActionsToProps = {
    getBiometricsLatest,
    getWeightTrend
};

const mapStateToProps = (state) => {
    return {
        dashboard: state.dashboard
    };
};

export default connect(mapStateToProps, mapActionsToProps)(WeightPoundsForm)