import React, {Component} from 'react'
import {NavLink, Redirect} from "react-router-dom";
import {Form, Button, Col} from 'react-bootstrap'
import {Formik} from "formik";
import * as Yup from 'yup'
import apiClient from '../../helpers/apiClient'

import {connect} from 'react-redux'
import {setUser} from "../../store/actions/auth";
import Alert from "react-bootstrap/Alert";

class UnitsForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasUpdated: false
        }

    }


    render() {

        let self = this;
        let userValues = {
            weight_unit: this.props.auth.user.weight_unit,
            glucose_unit: this.props.auth.user.glucose_unit
        };

        return (
            <Formik
                enableReinitialize={true}
                initialValues={userValues}
                onSubmit={(values, {setSubmitting}) => {
                    apiClient.put("/user/units/", values).then(function (response) {
                        //returns user, may as well set the store with it
                        self.props.setUser(response.data);
                        setSubmitting(false);
                        self.setState({
                            hasUpdated: true
                        });
                        let timeout = setTimeout(function () {
                            self.setState({
                                hasUpdated: false
                            });
                            clearTimeout(timeout);
                        }.bind(self), 4000);

                    });
                }}
                validationSchema={Yup.object().shape({
                    weight_unit: Yup.string()
                        .required('Required'),
                    glucose_unit: Yup.string()
                        .required('Required')
                })}
            >
                {props => {
                    const {
                        values,
                        touched,
                        errors,
                        //dirty,
                        isSubmitting,
                        handleChange,
                        //handleBlur,
                        handleSubmit,
                        //handleReset,
                    } = props;
                    return (
                        <Form noValidate onSubmit={handleSubmit}>

                            <Form.Group>
                                <Form.Label>I weigh with...</Form.Label>
                                <Form.Check
                                    type="radio"
                                    label="Kilograms"
                                    name="weight_unit"
                                    id="weight_kilograms"
                                    value="KILOGRAMS"
                                    checked={values.weight_unit === 'KILOGRAMS'}
                                    onChange={handleChange}
                                />
                                <Form.Check
                                    type="radio"
                                    label="Pounds"
                                    name="weight_unit"
                                    id="weight_pounds"
                                    value="POUNDS"
                                    checked={values.weight_unit === 'POUNDS'}
                                    onChange={handleChange}
                                />
                                <Form.Check
                                    type="radio"
                                    label="Stones & pounds"
                                    name="weight_unit"
                                    id="weight_stones_pounds"
                                    value="STONES_POUNDS"
                                    checked={values.weight_unit === 'STONES_POUNDS'}
                                    onChange={handleChange}
                                />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>I measure my blood sugar with...</Form.Label>
                                <Form.Check
                                    type="radio"
                                    label="mmol/L"
                                    name="glucose_unit"
                                    id="glucose_unit_mmoll"
                                    value="MMOLL"
                                    checked={values.glucose_unit === 'MMOLL'}
                                    onChange={handleChange}
                                />
                                <Form.Check
                                    type="radio"
                                    label="mg/dL"
                                    name="glucose_unit"
                                    id="glucose_unit_mgdl"
                                    value="MGDL"
                                    checked={values.glucose_unit === 'MGDL'}
                                    onChange={handleChange}
                                />
                            </Form.Group>


                            <Button
                                className="float-right"
                                type="submit" disabled={isSubmitting}>
                                Save
                            </Button>
                            <div className="clearfix"></div>
                            {
                                this.state.hasUpdated ?
                                    (
                                        <Alert className="mb-0 mt-3" variant="success">
                                           Updated
                                        </Alert>
                                    ): (null)
                            }



                        </Form>
                    );
                }}
            </Formik>
        )
    }
}

// start of code change
const mapStateToProps = (state) => {
    return {auth: state.auth};
};

export default connect(mapStateToProps, {setUser})(UnitsForm)