import React, {Component} from 'react'
import {NavLink, Redirect} from "react-router-dom";
import {Form, Button, Col} from 'react-bootstrap'
import {Formik} from "formik";
import * as Yup from 'yup'
import apiClient from '../../helpers/apiClient'

import {connect} from 'react-redux'
import {loginUser} from "../../store/actions/auth";
import Alert from "react-bootstrap/Alert";

class RegisterForm extends Component {
    constructor(props) {
        super(props);


    }


    render() {
        return (
            <Formik
                initialValues={{
                    first_name: '',
                    last_name: '',
                    gender: 'M',
                    email: '',
                    password: ''
                }}
                onSubmit={(values, {setSubmitting}) => {
                    let self = this;
                    apiClient.post("/register/", values)
                        .then(function () {
                            self.props.loginUser({
                                email: values.email,
                                password: values.password
                            });
                            setSubmitting(false);
                        })
                        .catch(function (error) {
                            console.log(error);
                            setSubmitting(false);
                        })


                }}
                validationSchema={Yup.object().shape({
                    first_name: Yup.string()
                        .required('Required'),
                    last_name: Yup.string()
                        .required('Required'),
                    email: Yup.string()
                        .email()
                        .required('Required'),
                    password: Yup.string()
                        .required('No password provided.')
                        .min(8, 'Password is too short - should be 8 chars minimum.')
                })}
            >
                {props => {
                    const {
                        values,
                        touched,
                        errors,
                        //dirty,
                        isSubmitting,
                        handleChange,
                        //handleBlur,
                        handleSubmit,
                        //handleReset,
                    } = props;
                    return (
                        <Form noValidate onSubmit={handleSubmit}>

                            <Form.Group>
                                <Form.Label>First name</Form.Label>
                                <Form.Control
                                    type="text"
                                    name="first_name"
                                    value={values.first_name}
                                    onChange={handleChange}
                                    isValid={!errors.first_name && touched.first_name}
                                    isInvalid={!!errors.first_name && touched.first_name}
                                />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Surname</Form.Label>
                                <Form.Control
                                    type="text"
                                    name="last_name"
                                    value={values.last_name}
                                    onChange={handleChange}
                                    isValid={!errors.last_name && touched.last_name}
                                    isInvalid={!!errors.last_name && touched.last_name}
                                />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Email</Form.Label>
                                <Form.Control
                                    type="email"
                                    name="email"
                                    value={values.email}
                                    onChange={handleChange}
                                    isValid={!errors.email && touched.email}
                                    isInvalid={!!errors.email && touched.email}
                                />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    name="password"
                                    value={values.password}
                                    onChange={handleChange}
                                    isInvalid={!!errors.password && touched.password}
                                />
                                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Gender</Form.Label>
                                <Form.Check
                                    type="radio"
                                    label="Male"
                                    name="gender"
                                    id="gender_male"
                                    value="M"
                                    onChange={handleChange}
                                />
                                <Form.Check
                                    type="radio"
                                    label="Female"
                                    name="gender"
                                    id="Female"
                                    value="F"
                                    onChange={handleChange}
                                />
                            </Form.Group>

                            <Button
                                className="float-right"
                                type="submit" disabled={isSubmitting}>
                                Register
                            </Button>

                            <div className="clearfix"></div>
                        </Form>
                    );
                }}
            </Formik>
        )
    }
}

// start of code change
const mapStateToProps = (state) => {
    return {auth: state.auth};
};

export default connect(mapStateToProps, {loginUser})(RegisterForm)