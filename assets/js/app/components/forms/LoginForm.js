import React, {Component} from 'react'
import {NavLink} from "react-router-dom";
import {Form, Button, Alert} from 'react-bootstrap'
import {Formik} from "formik";
import * as Yup from 'yup'
import {connect} from 'react-redux'
import {loginUser} from "../../store/actions/auth";


class LoginForm extends Component {
    render() {


        return (
            <Formik
                initialValues={{
                    email: '',
                    password: ''
                }}
                onSubmit={(values, {setSubmitting}) => {
                    this.props.loginUser(values, this.props);
                    setSubmitting(false);
                }}
                validationSchema={Yup.object().shape({
                    email: Yup.string()
                        .email()
                        .required('Required'),
                    password: Yup.string()
                        .required('No password provided.')
                        .min(8, 'Password is too short - should be 8 chars minimum.')
                })}
            >
                {props => {
                    const {
                        values,
                        touched,
                        errors,
                        //dirty,
                        isSubmitting,
                        handleChange,
                        //handleBlur,
                        handleSubmit,
                        //handleReset,
                    } = props;
                    return (
                        <Form noValidate onSubmit={handleSubmit}>
                            <Form.Group>
                                <Form.Label>Email</Form.Label>
                                <Form.Control
                                    type="email"
                                    name="email"
                                    value={values.email}
                                    onChange={handleChange}
                                    isValid={!errors.email && touched.email}
                                    isInvalid={!!errors.email}
                                />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    name="password"
                                    value={values.password}
                                    onChange={handleChange}
                                    isInvalid={!!errors.password && touched.password}
                                />
                                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                            </Form.Group>


                            {this.props.auth.isCredentialError
                                ? (
                                    <Alert variant="danger">Incorrect credentials</Alert>
                                ) : null
                            }

                            <Button
                                className="float-right"
                                type="submit" disabled={this.props.auth.isFormSubmitting}>
                                Submit
                            </Button>
                            <div className="clearfix"></div>
                        </Form>
                    );
                }}
            </Formik>
        )
    }
}

// start of code change
const mapStateToProps = (state) => {
    return { auth: state.auth};
};

export default connect(mapStateToProps, {loginUser})(LoginForm)