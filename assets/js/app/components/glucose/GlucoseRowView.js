import moment from 'moment'
import React, {Component} from 'react';

import {connect} from 'react-redux'

import PropTypes from 'prop-types'
import apiClient from '../../helpers/apiClient'
import swal from 'sweetalert'

import {DropdownButton, Dropdown} from 'react-bootstrap'
import {mgdlToMmoll} from "../../helpers/converters";

class GlucoseRowView extends Component {

    constructor(props) {
        super(props);
    }

    removeConfirm() {
        let self = this;
        swal({
            text: "Are you sure?",
            title: "Removing a glucose entry",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            closeModal: false,
        })
            .then((confirm) => {
                if (confirm) {
                    return apiClient.delete("/biometric/glucose/" + self.props.data.id);
                }
            })
            .then(function (response) {
                console.log(response.data);
                swal.stopLoading();
                swal.close();

                self.props.onDelete(self.props.data.id);
                swal("Success!", "Glucose entry removed", "success");
            })
    }

    render() {

        let conversion = mgdlToMmoll(this.props.data.mgdl), userUnitDisplay;
        switch (this.props.auth.user.glucose_unit) {
            case 'MMOLL':
                userUnitDisplay = conversion.toPrecision(2) + " mmol/L";
                break;
            default:
                userUnitDisplay = this.props.data.mgdl.toFixed(0) + ' mg/dL';
                break;
        }

        return (
            <div className="list-group-item mb-0 border-light border-left-0 border-right-0 border-top-0">
                <div className="row">
                    <div className="col-6">
                        <div className="h6 text-secondary">
                            {userUnitDisplay}
                        </div>
                        <div>
                            {moment(this.props.data.created_at).format('HH:MM a')}
                        </div>
                    </div>
                    <div className="col-6" align="right">
                        <DropdownButton size="sm" id="dropdown-item-button" title="Actions" variant="light" alignRight>
                            <Dropdown.Item as="button">Modify</Dropdown.Item>
                            <Dropdown.Item as="button" onClick={this.removeConfirm.bind(this)}>Remove</Dropdown.Item>
                        </DropdownButton>
                    </div>
                </div>
            </div>
        );
    }
}

GlucoseRowView.propTypes = {
    preference: PropTypes.string,
    data: PropTypes.object,
    onDelete: PropTypes.func,
    onUpdate: PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
};

export default connect(mapStateToProps, {}) (GlucoseRowView);
