import React, {Component} from 'react';
import moment from 'moment'
import PropTypes from 'prop-types';
import {DateRangePicker, SingleDatePicker, DayPickerRangeController} from 'react-dates';

import apiClient from '../../helpers/apiClient'
import Dygraph from 'dygraphs'

class GlucoseLine extends Component {

    constructor(props) {
        super(props)

        this.state = {
            focusedInput: null,
            startDate: moment().startOf('month'),
            endDate: moment().endOf('month')
        };
    }

    componentDidMount() {

        let self = this;
        apiClient.get("/biometric/glucose/between", {
            params: {
                from_date: self.state.startDate.toISOString(),
                to_date: self.state.endDate.toISOString(),
                per_page: 1000
            }
        })
            .then(function (response) {

                let data = [];
                response.data.data.forEach(function (item) {
                    data.push([moment(item.created_at).toDate(), item.mgdl]);
                })
                new Dygraph(self.refs.chart, data, {
                    /* options */
                });
                console.log(response.data)
            })


    }

    render() {
        return (
            <div className="card border-0 mb-4 mt-2">
                <div className="card-body">
                    <div style={{width: '100%'}} ref="chart"></div>
                    <div className="mt-3">
                        <DateRangePicker
                            minDate={null}
                            isOutsideRange={day => (moment().diff(day) < 0)}
                            startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                            endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                            endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                            onDatesChange={({startDate, endDate}) => this.setState({
                                startDate,
                                endDate
                            })} // PropTypes.func.isRequired,
                            focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                            onFocusChange={focusedInput => this.setState({focusedInput})} // PropTypes.func.isRequired,
                        />
                    </div>
                </div>
            </div>
        );

    }
}

GlucoseLine.propTypes = {};

export default GlucoseLine;
