import '../../../../css/glucosePagelet.scss'
import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Card, Button, Modal} from 'react-bootstrap'

const bloodDrop = require('../../../../img/blood-drop.svg');

import Trend from 'react-trend';
import {getGlucoseTrend} from "../../store/actions/glucose";
import GlucoseMgdlForm from "../forms/biometrics/create/GlucoseMgdlForm";
import GlucoseMmollForm from '../forms/biometrics/create/GlucoseMmollForm';

import {NavLink} from "react-router-dom";
import _ from "lodash";
import {mgdlToMmoll} from "../../helpers/converters";

class GlucosePagelet extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false
        };

        this.handleModalShow = this.handleModalShow.bind(this);
        this.handleModalClose = this.handleModalClose.bind(this);
    }

    componentWillMount() {
        this.props.getGlucoseTrend();
    }

    handleModalShow() {
        this.setState({
            isModalOpen: true
        })
    }

    handleModalClose() {
        this.setState({
            isModalOpen: false
        })
    }

    renderForm() {
        if (_.has(this.props.auth.user, 'glucose_unit')) {
            switch (this.props.auth.user.glucose_unit) {
                case 'MGDL':
                    return <GlucoseMgdlForm onSave={this.handleModalClose}/>;
                case 'MMOLL':
                    return <GlucoseMmollForm onSave={this.handleModalClose}/>;
                default:
                    return <GlucoseMgdlForm onSave={this.handleModalClose}/>
            }
        }
    }

    render() {
        let data, hasData = false, userUnitDisplay;
        if (this.props.data.length > 0) {
            hasData = true;
            data = this.props.data[0];
            let conversion = mgdlToMmoll(data.mgdl);
            switch (this.props.auth.user.glucose_unit) {
                case 'MMOLL':
                    userUnitDisplay = conversion.toPrecision(2) + " mmol/L";
                    break;
                default:
                    userUnitDisplay = data.mgdl.toFixed(0) + ' mg/dL';
                    break;
            }
        }
        return (
            <div>
                <Card.Body className="rounded border-0 glucosePagelet mb-3">
                    <div className="media">
                        <img className="mr-3" src={bloodDrop} width="22" height="28"/>
                        <div className="media-body">
                            <div className="row">
                                <div className="col">
                                    <div>Glucose</div>
                                    <h4 className="statTitle pb-0">
                                        {hasData ? (
                                            <span>{userUnitDisplay}</span>
                                        ) : (
                                            <span>No data</span>
                                        )}
                                    </h4>
                                </div>
                                <div className="col" align="right">
                                    {hasData ? (
                                        <Trend
                                            smooth
                                            radius={20}
                                            stroke="rgba(181,28,28,1)"
                                            strokeWidth={4}
                                            height={60}
                                            data={this.props.glucose.trend}
                                        />
                                    ) : (
                                        <Trend
                                            smooth
                                            radius={20}
                                            stroke="rgba(181,28,28,1)"
                                            strokeWidth={4}
                                            height={60}
                                            data={[]}
                                        />
                                    )}
                                </div>
                            </div>
                            <Button
                                onClick={this.handleModalShow.bind(this)}
                                variant="outline-primary"
                                size="sm">Add</Button>

                            <NavLink to="/glucose" className="btn btn-sm btn-link float-right">
                                See all
                            </NavLink>
                            <div className="clearfix"></div>
                            <Modal centered show={this.state.isModalOpen} onHide={this.handleModalClose}>
                                <Modal.Header closeButton>
                                    <Modal.Title as="h5">New reading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    {this.renderForm()}
                                </Modal.Body>
                            </Modal>

                        </div>
                    </div>
                </Card.Body>
            </div>
        );
    }
}

GlucosePagelet.propTypes = {
    data: PropTypes.array
};

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        glucose: state.glucose
    }
};

export default connect(mapStateToProps, {getGlucoseTrend})(GlucosePagelet);
