import '../../../../css/weightPagelet.scss'
import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {connect} from "react-redux";
import {Button, Card, Modal} from 'react-bootstrap'
import _ from 'lodash'

const weightScale = require('../../../../img/weight-scale.svg');
import Trend from 'react-trend'
import {getWeightTrend} from "../../store/actions/weight";
import {kilogramsToPounds, kilogramsToStonesAndPounds} from "../../helpers/converters";
import WeightKilogramsForm from "../forms/biometrics/create/WeightKilogramsForm";
import WeightStonesPoundsForm from "../forms/biometrics/create/WeightStonesPoundsForm";
import WeightPoundsForm from "../forms/biometrics/create/WeightPoundsForm";
import {NavLink} from "react-router-dom";

class WeightPagelet extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false
        };
        this.handleModalShow = this.handleModalShow.bind(this);
        this.handleModalClose = this.handleModalClose.bind(this);
    }

    componentWillMount() {
        this.props.getWeightTrend();
    }

    handleModalShow() {
        this.setState({
            isModalOpen: true
        })
    }

    handleModalClose() {
        this.setState({
            isModalOpen: false
        })
    }

    renderForm() {
        if (_.has(this.props.auth.user, 'weight_unit')) {
            switch (this.props.auth.user.weight_unit) {
                case 'KILOGRAMS':
                    return <WeightKilogramsForm onSave={this.handleModalClose}/>;
                case 'STONES_POUNDS':
                    return <WeightStonesPoundsForm onSave={this.handleModalClose}/>;
                case 'POUNDS':
                    return <WeightPoundsForm onSave={this.handleModalClose}/>
                default:
                    return <WeightKilogramsForm onSave={this.handleModalClose}/>
            }
        }
    }

    render() {
        let data, hasData = false, userUnitDisplay;
        if (this.props.data.length > 0) {
            hasData = true; data = this.props.data[0];
            let conversion = kilogramsToStonesAndPounds(data.kilograms);
            switch (this.props.auth.user.weight_unit) {
                case 'STONES_POUNDS':
                    userUnitDisplay = conversion.stones + " st " + conversion.pounds + " lb";
                    break;
                case 'KILOGRAMS':
                    userUnitDisplay = parseInt(data.kilograms).toFixed(0) + " kg";
                    break;
                case 'POUNDS':
                    userUnitDisplay = kilogramsToPounds(data.kilograms).toFixed(0) + " lbs";
                    break;
                default:
                    userUnitDisplay = data.kilograms + 'kg';
                    break;
            }
        }

        return (
            <div>
                <Card.Body className="rounded border-0 weightPagelet">
                    <div className="media">
                        <img className="mr-3" src={weightScale} width="22"/>
                        <div className="media-body">
                            <div className="row">
                                <div className="col">
                                    <div>Weight</div>
                                    <h4 className="statTitle pb-0">
                                        {hasData ? (
                                            <span>
                                                {userUnitDisplay}
                                                </span>
                                        ) : (
                                            <span>No data</span>
                                        )}
                                    </h4>
                                </div>
                                <div className="col" align="right">
                                    {hasData ? (
                                        <Trend
                                            smooth
                                            radius={20}
                                            stroke="#3E8FAB"
                                            strokeWidth={4}
                                            height={60}
                                            data={this.props.weight.trend}
                                        />
                                    ) : (
                                        <Trend
                                            smooth
                                            radius={20}
                                            stroke="white"
                                            strokeWidth={4}
                                            height={60}
                                            data={[]}
                                        />
                                    )}
                                </div>
                            </div>
                            <Button
                                onClick={this.handleModalShow.bind(this)}
                                variant="outline-primary"
                                size="sm">Add</Button>

                            <NavLink to="/weight" className="btn btn-sm btn-link float-right">
                                See all
                            </NavLink>
                            <Modal centered show={this.state.isModalOpen} onHide={this.handleModalClose}>
                                <Modal.Header closeButton>
                                    <Modal.Title as="h5">New reading</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    {this.renderForm()}
                                </Modal.Body>
                            </Modal>
                        </div>
                    </div>
                </Card.Body>
            </div>
        );
    }
}

WeightPagelet.propTypes = {
    data: PropTypes.array
};

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        weight: state.weight
    }
};

export default connect(mapStateToProps, {getWeightTrend})(WeightPagelet);