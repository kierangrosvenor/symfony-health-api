import React, {Component} from 'react'
import {withRouter} from "react-router-dom"
import _ from 'lodash'
import {Navbar, Nav, Container} from 'react-bootstrap';
import {connect} from "react-redux";
import {LinkContainer} from "react-router-bootstrap";
import NavLink from "react-bootstrap/NavLink";
import {logoutUser} from "../store/actions/auth";
import NavDropdown from "react-bootstrap/NavDropdown";

class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            navExpanded: false
        };
    }

    setNavExpanded(expanded) {
        this.setState({navExpanded: expanded});
    }

    closeNav() {
        this.setState({navExpanded: false});
    }

    logOut() {
        this.closeNav();
        this.props.logoutUser();
    }

    render() {
        let routes = [];
        let loggedIn = !_.isEmpty(this.props.auth.user);
        if (!loggedIn) {
            routes = [
                {
                    name: "Home",
                    to: "/",
                    exact: true
                },
                {
                    name: "Register",
                    to: "/register",

                },
                {
                    name: "Login",
                    to: "/login",
                }
            ]
        } else {
            routes = [
                {
                    name: "Dashboard",
                    to: "/dashboard",
                }
            ]
        }
        return (
            <div>
                <Navbar className="navbar-dark bg-primary"  expand="lg" collapseOnSelect onToggle={this.setNavExpanded.bind(this)}
                        expanded={this.state.navExpanded}>



                    <Container>

                        <Navbar.Brand href="#home">HealthApp</Navbar.Brand>
                        <Navbar.Toggle/>
                        <Navbar.Collapse>
                            <Nav onSelect={this.closeNav.bind(this)} className="ml-auto">
                                {routes.map((item) => {
                                    return (
                                        <LinkContainer key={item.name} to={item.to} onClick={this.closeNav.bind(this)}>
                                            <NavLink>{item.name}</NavLink>
                                        </LinkContainer>
                                    )
                                })}
                                {loggedIn ?
                                    (
                                        <NavDropdown alignRight title={this.props.auth.user.first_name}>
                                            <LinkContainer to="/settings" onClick={this.closeNav.bind(this)}>
                                                <NavDropdown.Item>
                                                    Settings
                                                </NavDropdown.Item>
                                            </LinkContainer>
                                            <NavDropdown.Divider/>

                                            <NavDropdown.Item onClick={this.logOut.bind(this)}>Logout</NavDropdown.Item>
                                        </NavDropdown>
                                    ) : null}
                            </Nav>
                        </Navbar.Collapse>

                    </Container>



                </Navbar>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {auth: state.auth};
};

export default withRouter(connect(mapStateToProps, {logoutUser})(Header));