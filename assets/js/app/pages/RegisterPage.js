import React, {Component} from 'react';

import RegisterForm from '../components/forms/RegistrationForm'
import {NavLink} from "react-router-dom";

class RegisterPage extends Component {
    render() {
        return (
            <div>
                <div>
                    <div className="row">
                        <div className="col-10 col-md-4 col-lg-4 col-sm-12 ml-auto mr-auto p-4">
                            <h1>Sign up</h1>
                            <p>Track your health in the cloud</p>
                            <RegisterForm/>
                            <div className="mt-3">
                                Already have an account? <NavLink to="/login">Sign in</NavLink>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default RegisterPage;