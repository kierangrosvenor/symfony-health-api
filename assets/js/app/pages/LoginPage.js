import React, {Component} from 'react';
import LoginForm from "../components/forms/LoginForm";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {NavLink} from "react-router-dom";
import {Form} from "react-bootstrap";

class LoginPage extends Component {
    render() {
        return (
            <div>
                <div className="row">


                    <div className="col-10 col-md-4 col-lg-4 col-sm-12 ml-auto mr-auto p-4">
                        <h1>Sign in</h1>
                        <p>Continue tracking your health</p>
                        <LoginForm/>
                        <div className="mt-3">
                            Don't have an account yet? <NavLink to="/register">Get one</NavLink>
                        </div>
                    </div>
                </div>


            </div>
        )
    }
}

export default LoginPage;