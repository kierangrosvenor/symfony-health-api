import React, {Component} from 'react';
import RegisterForm from "../components/forms/RegistrationForm";
import LoginForm from "../components/forms/LoginForm";
import Container from "react-bootstrap/Container";
import {Redirect} from "react-router-dom";
import {connect} from "react-redux";

class HomePage extends Component {
    render(){

        if(this.props.auth.user.id) {
            return <Redirect to="/dashboard"/>
        }


        return (
            <div>

                <Container className="pt-4">

                    <div className="row">

                        <div className="col-6 ml-auto mr-auto">
                            <h2>Get started</h2>
                            <p>HealthApp is a free open source health tracking service</p>
                            <RegisterForm/>
                        </div>
                    </div>
                </Container>


            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps, {})(HomePage);