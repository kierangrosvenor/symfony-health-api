import React, {Component} from 'react';
import _ from 'lodash'
import moment from 'moment'
import {Container, Row, Col, Button} from "react-bootstrap";
import apiClient from '../helpers/apiClient'
import {connect} from 'react-redux'
import GlucoseRowView from "../components/glucose/GlucoseRowView";
import GlucoseLine from "../components/graphs/glucoseLine"
import GlucoseMgdlForm from '../components/forms/biometrics/create/GlucoseMgdlForm'

class GlucosePage extends Component {

    constructor() {
        super();
        this.state = {
            paginationInfo: {
                pages: 0,
                next: 1
            },
            data: [],
            grouped: [],
            hasMoreItems: true
        }
    }

    componentWillMount() {
        this.loadData();
    }

    loadData() {
        let self = this;

        if (self.state.hasMoreItems === true) {
            apiClient.get('/biometric/glucose/list', {
                params: {
                    sort: "DESC",
                    page: self.state.paginationInfo.next
                }
            })
                .then(function (response) {
                    let data = self.state.data;
                    data.push.apply(data, response.data.data);
                    let hasMoreItems = _.has(response.data.pagination_info, 'next');
                    self.setState({
                        hasMoreItems: hasMoreItems,
                        paginationInfo: response.data.pagination_info,
                        data: data,
                        grouped: self.groupByMonth(data)
                    })
                })
        }
    }

    groupByMonth(data) {
        return _.groupBy(data, function (reading) {
            return moment(reading.created_at).startOf('month').format()
        });
    }

    onItemAdded(item) {
        let data = this.state.data;
        data.unshift(item);
        this.setState({
            data: data,
            grouped: this.groupByMonth(data)
        });
    }

    onItemDeleted(id) {
        //alert(id);
        let self = this;
        let data = self.state.data;
        let index = data.map(function (item) {
            return item.id
        }).indexOf(id);
        //remove / update state
        data.splice(index, 1);
        self.setState({
            data: data,
            grouped: self.groupByMonth(data)
        });
        //regroup
        self.groupByMonth(data);

    }

    render() {
        return (
            <Container className="pt-3">
                <GlucoseLine/>
                <Row className="mt-3">
                    <Col>
                        <div className="card border-light">
                            <div className="card-body">
                                <h4 className="font-weight-lighter mb-3">Log glucose</h4>
                                <GlucoseMgdlForm onSave={this.onItemAdded.bind(this)}/>
                            </div>
                        </div>
                    </Col>
                    <Col md="9" lg="9">
                        {Object.keys(this.state.grouped).map((group, i) => {
                            return (
                                <div key={i} className="mb-3">
                                    <div className="list-group border-0">
                                        <div className="list-group-item border-0 bg-secondary text-white">
                                            {moment(group).format('MMMM YYYY')}
                                        </div>
                                        {this.state.grouped[group].map((item) => (
                                            <GlucoseRowView
                                                onDelete={this.onItemDeleted.bind(this)}
                                                key={item.id}
                                                data={item}>
                                            </GlucoseRowView>
                                        ))}
                                    </div>
                                </div>
                            )
                        })}
                        <Button onClick={this.loadData.bind(this)}>Load more</Button>
                    </Col>
                </Row>
            </Container>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        state
    }
};

export default connect(mapStateToProps, {})(GlucosePage);
