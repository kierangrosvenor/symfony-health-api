import React, {Component} from 'react';
import {connect} from "react-redux";

import {getBiometricsLatest} from "../store/actions/dashboard";

import GlucosePagelet from "../components/dashboard/GlucosePagelet";
import WeightPagelet from "../components/dashboard/WeightPagelet";

import {Container, Nav, Card} from "react-bootstrap";
import WeightKilogramsForm from "../components/forms/biometrics/create/WeightKilogramsForm";
import GlucoseMgdlForm from "../components/forms/biometrics/create/GlucoseMgdlForm";


class DashboardPage extends Component {
    componentWillMount() {
        this.props.getBiometricsLatest();
    }

    render() {
        let hasData = false;
        if (this.props.dashboard.latestBiometrics !== null) {
            hasData = true;
        }

        return (
            <Container>
                <div className="row">
                    <div className="col-md-12">
                        <div className="pt-3">
                            {hasData ?
                                (
                                    <div className="mt-3">
                                        <div className="row">
                                            <div className="col-12 col-md-6 col-lg-6">
                                                <GlucosePagelet
                                                    data={
                                                        this
                                                            .props
                                                            .dashboard
                                                            .latestBiometrics
                                                            .latest_glucose
                                                    }
                                                />
                                            </div>
                                            <div className="col-12 col-md-6 col-lg-6">
                                                <WeightPagelet
                                                    data={
                                                        this
                                                            .props
                                                            .dashboard
                                                            .latestBiometrics
                                                            .latest_weight
                                                    }
                                                />
                                            </div>
                                        </div>
                                    </div>
                                ) : null}
                        </div>
                    </div>
                </div>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {auth: state.auth, dashboard: state.dashboard};
};

export default connect(mapStateToProps, {getBiometricsLatest})(DashboardPage)