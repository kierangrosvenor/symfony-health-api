import React, {Component} from 'react';
import _ from 'lodash'
import {connect} from "react-redux";
import {Container, Nav, Tab, Row, Col, Card} from "react-bootstrap";
import UnitsForm from "../components/forms/UnitsForm";

class SettingsPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tabs: [
                {
                    title: "Units",
                    eventKey: "units",
                    component: <UnitsForm/>
                }
            ]
        }
    }

    render() {
        return (
            <Container className="pt-3">
                <Tab.Container defaultActiveKey="units">
                    <Row  className="mt-3">
                        <Col sm={3}>
                            <Nav variant="pills" className="flex-column">
                                {this.state.tabs.map((tab) => {
                                    return (
                                        <Nav.Item key={tab.title}>
                                            <Nav.Link eventKey={tab.eventKey}>{tab.title}</Nav.Link>
                                        </Nav.Item>
                                    )
                                })}
                            </Nav>
                        </Col>
                        <Col sm={9}>
                            <Tab.Content>
                                {this.state.tabs.map((tab) => {
                                    return (
                                        <Tab.Pane key={tab.title} eventKey={tab.eventKey}>
                                            <Card>
                                                <Card.Body>
                                                    {tab.component}
                                                </Card.Body>
                                            </Card>
                                        </Tab.Pane>
                                    )
                                })}
                            </Tab.Content>
                        </Col>
                    </Row>
                </Tab.Container>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps, {})(SettingsPage);