import React, {Component} from 'react'
import {Provider} from 'react-redux'
import store from './store/store'
import 'react-dates/initialize';
import createHashHistory from 'history/createHashHistory'
import Routes from "./routes";
import Cookies from "js-cookie";
import {getUser, setToken} from "./store/actions/auth";

class App extends Component {
    constructor(props) {
        super(props);
    }
    componentWillMount() {
        let token = Cookies.get('token');
        store.dispatch(setToken(token ? token: null));
        store.dispatch(getUser(false));
    }

    render() {
        return (
            <Provider store={store}>
                <div>
                    <Routes/>
                </div>
            </Provider>
        )
    }
}

export const history = createHashHistory();
export default App;