<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 09/02/2019
 * Time: 01:21
 */

namespace App\DTO\Biometrics;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation AS JMS;

class WeightDTO {


    /**
     * @JMS\Type("double")
     * @Assert\NotBlank
     */
    private $kilograms;

    /**
     * @JMS\SkipWhenEmpty
     * @JMS\Type("string")
     */
    private $notes;

    /**
     * @JMS\Type("DateTime")
     * @Assert\NotBlank
     */
    private $createdAt;


    public function getKilograms() : string {
        return $this->kilograms;
    }


    public function getNotes() : string {
        return $this->notes;
    }

    public function getCreatedAt()  : \DateTimeInterface {
        return $this->createdAt;
    }

}