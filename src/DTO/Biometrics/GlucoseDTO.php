<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 09/02/2019
 * Time: 01:21
 */

namespace App\DTO\Biometrics;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation AS JMS;

class GlucoseDTO
{

    /**
     * @JMS\Type("double")
     * @Assert\NotBlank
     */
    private $mgdl;

    /**
     * @JMS\Type("string")
     * @Assert\NotBlank
     * @Assert\Choice(choices={"BEFORE_MEAL", "AFTER_MEAL"}, message="Before or after meal?"))
     **/
    private $mealStatus;


    /**
     * @JMS\Type("string")
     */
    private $notes;

    /**
     * @JMS\Type("DateTime")
     * @Assert\NotBlank
     */
    private $createdAt;

    public function getMealStatus(): string
    {
        return $this->mealStatus;
    }


    public function getMgdl(): string
    {
        return $this->mgdl;
    }


    public function getNotes(): string
    {
        return $this->notes;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }
}