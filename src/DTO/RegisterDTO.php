<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 09/02/2019
 * Time: 00:16
 */

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation AS JMS;

class RegisterDTO
{

    /**
     * @JMS\Type("string")
     * @Assert\NotBlank
     **/
    private $firstName;

    /**
     * @JMS\Type("string")
     * @Assert\NotBlank
     **/
    private $lastName;

    /**
     * @JMS\Type("string")
     * @Assert\NotBlank
     **/
    private $email;

    /**
     * @JMS\Type("string")
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 6
     * )
     **/
    private $password;

    /**
     * @JMS\Type("string")
     * @Assert\NotBlank
     * @Assert\Choice(choices={"M", "F"}, message="Choose your sex"))
     **/
    private $gender;


    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getGender() : string {
        return $this->gender;
    }


}