<?php

namespace App\DTO\Security;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation AS JMS;

class CredentialLoginDTO
{
    /**
     * CredentialLoginDTO constructor.
     * @param string $email
     * @param string $password
     */
    public function __construct(string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @JMS\Type("string")
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     * @Assert\NotBlank
     **/
    private $email;

    /**
     * @JMS\Type("string")
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 6
     * )
     */
    private $password;

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}