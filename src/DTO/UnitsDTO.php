<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 09/02/2019
 * Time: 00:16
 */

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation AS JMS;

class UnitsDTO
{

    /**
     * @JMS\Type("string")
     * @Assert\NotBlank
     * @Assert\Choice(choices={"KILOGRAMS", "POUNDS", "STONES_POUNDS"}))
     **/
    private $weightUnit;

    /**
     * @JMS\Type("string")
     * @Assert\NotBlank
     * @Assert\Choice(choices={"MGDL", "MMOLL"}))
     **/
    private $glucoseUnit;


    public function getWeightUnit(): string
    {
        return $this->weightUnit;
    }

    public function getGlucoseUnit(): string
    {
        return $this->glucoseUnit;
    }

}