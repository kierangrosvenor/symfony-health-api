<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

use JMS\Serializer\Annotation AS JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @JMS\Exclude
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *     min = 6
     * )
     * @JMS\Exclude
     * @ORM\Column(type="string")
     */
    private $password;

    private $plainPassword;

    /**
     * @Assert\NotBlank(message="Add your first name")
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $firstName;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=150)
     */
    private $lastName;

    /**
     * @Assert\NotBlank
     * @Assert\Choice(choices={"M", "F"}, message="Choose your sex"))
     * @ORM\Column(type="string", length=1)
     */
    private $gender;

    /**
     * @Assert\Choice(choices={"KILOGRAMS", "POUNDS", "STONES_POUNDS"}))
     * @ORM\Column(type="string", length=20,  columnDefinition="ENUM('KILOGRAMS', 'POUNDS', 'STONES_POUNDS')")
     */
    private $weightUnit = "KILOGRAMS";

    /**
     * @Assert\Choice(choices={"MGDL", "MMOLL"}))
     * @ORM\Column(type="string", length=5,  columnDefinition="ENUM('MGDL', 'MMOLL')")
     */
    private $glucoseUnit = "MGDL";


    /**
     * @JMS\Exclude
     * @ORM\Column(type="string", nullable=true)
     */
    private $activationToken;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEmailActivated = 0;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $activatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }


    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    public function setActivationToken(string $token) : self {
        $this->activationToken = $token;
        return $this;
    }

    public function getActivationToken() : string {
        return $this->activationToken;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {

    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt) : self {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getCreatedAt() : \DateTimeInterface {
        return $this->createdAt;
    }

    public function setActivatedAt(\DateTimeInterface $activatedAt) : self {
        $this->activatedAt = $activatedAt;
        return $this;
    }

    public function getActivatedAt() : \DateTimeInterface {
        return $this->activatedAt;
    }

    public function setIsEmailActivated(bool $isEmailActivated) : self {
        $this->isEmailActivated = $isEmailActivated;
        return $this;
    }

    public function getIsEmailActivated() : bool {
        return $this->isEmailActivated;
    }

    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        $this->password = null;
    }


    public function getWeightUnit(): string
    {
        return $this->weightUnit;
    }

    public function setWeightUnit(string $weightUnit): self
    {
        $this->weightUnit = $weightUnit;
        return $this;
    }


    public function getGlucoseUnit(): string
    {
        return $this->glucoseUnit;
    }

    public function setGlucoseUnit($glucoseUnit): self
    {
        $this->glucoseUnit = $glucoseUnit;
        return $this;
    }

}
