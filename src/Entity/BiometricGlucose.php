<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation AS JMS;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\BiometricGlucoseRepository")
 */
class BiometricGlucose
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @JMS\Exclude
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;


    /**
     * @ORM\Column(type="decimal", precision=7, scale=3)
     */
    private $mgdl;


    /**
     * @ORM\Column(type="string", length=20,  columnDefinition="ENUM('BEFORE_MEAL', 'AFTER_MEAL')")
     */
    private $mealStatus;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="datetime")
     * @JMS\Type("DateTime<'c'>")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Type("DateTime<'c'>")
     */
    private $updatedAt;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function setNotes(string $notes): self
    {
        $this->notes = $notes;
        return $this;
    }

    public function getNotes() : string {
        return $this->notes;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getMgdl()
    {
        return $this->mgdl;
    }

    public function setMgdl($mgdl): self
    {
        $this->mgdl = $mgdl;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getMealStatus(): ?string
    {
        return $this->mealStatus;
    }

    public function setMealStatus(string $mealStatus): self
    {
        $this->mealStatus = $mealStatus;

        return $this;
    }
}
