<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 12/02/2019
 * Time: 23:51
 */

namespace App\Consumer;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Routing\RouterInterface;

class EmailActivationConsumer implements ConsumerInterface
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;


    /**
     * @var \Swift_Message
     */
    private $message;

    public function __construct(
        \Swift_Mailer $mailer,
        \Twig_Environment $twig,
        RouterInterface $router,
        UserRepository $userRepository,
        EntityManagerInterface $em
    )
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->router = $router;
        $this->userRepository = $userRepository;
        $this->em = $em;
        $this->message = new \Swift_Message();
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to reject
     */
    public function execute(AMQPMessage $msg)
    {

        $data = json_decode($msg->body);
        if (isset($data->id)) {
            $user = $this->userRepository->find($data->id);
            if (!$user) {
                //user not found, discard message
                return "reject";
            }
            try {
                $activationToken = $this->generateActivationToken();
                $user->setActivationToken($activationToken);
                $this->em->persist($user);
                $this->em->flush();
                $this->message->setSubject('Welcome to HealthApp, please confirm your email address')
                    ->setFrom("postmaster@" . getenv('MAILGUN_DOMAIN'))
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->twig->render("emails/email_verification.html.twig", [
                            'first_name' => $user->getFirstName(),
                            'token' => $this->router->generate("app.activate_email", [
                                'at' => $activationToken
                            ])
                        ]),
                        'text/html'
                    );
                try {
                    $this->mailer->send($this->message);
                } catch (\Exception $ex) {
                    return "reject";
                }

                $this->em->clear();
                $this->userRepository->clear();

                echo memory_get_usage() .PHP_EOL;

                return true;

            } catch (\Exception $ex) {
                return "reject";
            }
        } else {
            //data wasn't set
            return "reject";
        }
    }

    /**
     * @return string
     */
    private function generateActivationToken(): string
    {
        $strong = true;
        $bytes = openssl_random_pseudo_bytes(
            64,
            $strong
        );
        return bin2hex($bytes);
    }
}