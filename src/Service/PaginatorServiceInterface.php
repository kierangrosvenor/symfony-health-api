<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 11/02/2019
 * Time: 23:02
 */

namespace App\Service;


use Doctrine\ORM\QueryBuilder;

interface PaginatorServiceInterface
{
    /**
     * @param QueryBuilder $queryBuilder
     * @param int $page
     * @param int $perPage
     * @param string $sort
     * @return mixed
     */
    public function paginate(QueryBuilder $queryBuilder, int $page,  int $perPage, string $sort);
}