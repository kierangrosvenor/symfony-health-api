<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 11/02/2019
 * Time: 23:02
 */

namespace App\Service;

use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Doctrine\ORM\QueryBuilder;


final class PaginatorService implements PaginatorServiceInterface
{

    /**
     * @param QueryBuilder $queryBuilder
     * @param int $page
     * @param int $perPage
     * @param string $sort
     * @return mixed
     */
    public function paginate(QueryBuilder $queryBuilder, int $page, int $perPage, string $sort)
    {
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $fanta = new Pagerfanta($adapter);
        $fanta->setCurrentPage(
            $page
        );
        $fanta->setMaxPerPage(
            $perPage
        );
        $items = [];
        foreach ($fanta->getCurrentPageResults() as $item) {
            $items[] = $item;
        }
        return [
            'data' => $items,
            'pagination_info' => [
                'count' => $fanta->count(),
                'prev' => $fanta->hasPreviousPage() ? $fanta->getPreviousPage() : null,
                'next' => $fanta->hasNextPage() ? $fanta->getNextPage() : null,
                'pages' => $fanta->getNbPages()
            ],
        ];
    }
}