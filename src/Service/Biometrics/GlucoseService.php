<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 11/02/2019
 * Time: 00:22
 */

namespace App\Service\Biometrics;


use App\DTO\Biometrics\GlucoseDTO;
use App\DTO\Biometrics\WeightDTO;
use App\Entity\BiometricGlucose;
use App\Entity\BiometricWeight;
use App\Entity\User;
use App\Repository\BiometricGlucoseRepository;
use App\Repository\BiometricWeightRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

class GlucoseService implements GlucoseServiceInterface
{


    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BiometricGlucoseRepository
     */
    private $repository;


    /**
     * GlucoseServiceInterface constructor.
     * @param EntityManagerInterface $em
     * @param BiometricGlucoseRepository $repository
     */
    public function __construct(EntityManagerInterface $em, BiometricGlucoseRepository $repository)
    {
        $this->em = $em;
        $this->repository = $repository;
    }

    /**
     *  Read DTO and return and map to entity
     * @param GlucoseDTO $dto
     * @return BiometricGlucose
     */
    public function readDTO(GlucoseDTO $dto): BiometricGlucose
    {
        $glucose = new BiometricGlucose();
        $glucose->setMgdl($dto->getMgdl());
        $glucose->setCreatedAt($dto->getCreatedAt());
        $glucose->setMealStatus($dto->getMealStatus());
        $glucose->setNotes($dto->getNotes());
        return $glucose;
    }

    /**
     * Create entry
     * @param GlucoseDTO $dto
     * @param User $user
     * @return BiometricGlucose
     */
    public function create(GlucoseDTO $dto, User $user): BiometricGlucose
    {
        $glucose = $this->readDTO($dto);
        $glucose->setUser($user);
        $this->em->persist($glucose);
        $this->em->flush();
        return $glucose;
    }

    /**
     * Update an entry
     * @param GlucoseDTO $dto
     * @param int $id
     * @param User $user
     * @throws \Exception
     * @throws HttpException
     * @return BiometricGlucose
     */
    public function update(GlucoseDTO $dto, int $id, User $user): BiometricGlucose
    {
        $glucose = $this->repository->find($id);

        if (!isset($weight)) {
            throw new HttpException(404, "Entity not found");
        }

        if ($user->getId() !== $weight->getUser()->getId()) {
            throw new HttpException(401, "Permission denied");
        }

        $glucose->setUpdatedAt(new \DateTime());
        $glucose->setCreatedAt($dto->getCreatedAt());
        $glucose->setMealStatus($dto->getMealStatus());
        $glucose->setMgdl($dto->getMgdl());
        $glucose->setNotes($dto->getNotes());

        try {
            $this->em->persist($glucose);
            $this->em->flush();
            return $glucose;
        } catch (\Exception $ex) {
            throw new HttpException(500, "Database error");
        }
    }



    /**
     * @param int $id
     * @param User $user
     * @return bool
     */
    public function delete(int $id, User $user): bool
    {
        $weight = $this->repository->find($id);
        if (!isset($weight)) {
            throw new HttpException(404, "Entity not found");
        }
        if ($user->getId() !== $weight->getUser()->getId()) {
            throw new HttpException(402, "Permission denied");
        }
        try {
            $this->em->remove($weight);
            $this->em->flush();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param \DateTime $fromDate
     * @param \DateTime $toDate
     * @param int $userId
     * @param string $sort
     * @return array|mixed
     */
    public function calendarDateAggregation(\DateTime $fromDate, \DateTime $toDate, int $userId, string $sort = "ASC")
    {
        $dates = $this->repository->calendarDateAggregation($fromDate, $toDate, $userId, $sort);
        $formatted = [];
        foreach ($dates as $result) {
            $formatted[] = [
                'count' => (int)$result['count'],
                'date' => $result['createdAt']
            ];
        }
        return $formatted;
    }
}