<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 09/02/2019
 * Time: 10:40
 */

namespace App\Service\Biometrics;

use App\Entity\User;
use App\Repository\BiometricWeightRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\BiometricWeight;
use App\DTO\Biometrics\WeightDTO;
use Symfony\Component\HttpKernel\Exception\HttpException;

final class WeightService implements WeightServiceInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BiometricWeightRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $em, BiometricWeightRepository $repository)
    {
        $this->repository = $repository;
        $this->em = $em;
    }



    /**
     * Reads the Data Transfer Object and returns a new BiometicWeight
     * @param WeightDTO $dto
     * @return BiometricWeight
     */
    public function readDTO(WeightDTO $dto): BiometricWeight
    {
        $weight = new BiometricWeight();
        $weight->setCreatedAt($dto->getCreatedAt());
        $weight->setKilograms($dto->getKilograms());
        $weight->setNotes($dto->getNotes());
        return $weight;
    }

    /**
     * Adds a new weight entry, reads the DTO maps to Entity object and persist with ORM
     * @param WeightDTO $weightPostDTO
     * @param User $user
     * @return BiometricWeight
     */
    public function create(WeightDTO $weightPostDTO, User $user): BiometricWeight
    {
        $weight = $this->readDTO($weightPostDTO);
        $weight->setUser($user);

        $this->em->persist($weight);
        $this->em->flush();
        return $weight;
    }

    /**
     * @param \DateTime $fromDate
     * @param \DateTime $toDate
     * @param int $userId
     * @param string $sort
     * @return array|mixed
     */
    public function calendarDateAggregation(\DateTime $fromDate, \DateTime $toDate, int $userId, string $sort = "ASC")
    {
        $dates = $this->repository->calendarDateAggregation($fromDate, $toDate, $userId, $sort);
        $formatted = [];
        foreach ($dates as $result) {
            $formatted[] = [
                'count' => (int)$result['count'],
                'date' => $result['createdAt']
            ];
        }
        return $formatted;
    }

    /**
     * @param int $id
     * @param User $user
     * @return bool
     */
    public function delete(int $id, User $user): bool
    {
        $weight = $this->repository->find($id);
        if (!isset($weight)) {
            throw new HttpException(404, "Entity not found");
        }
        if ($user->getId() !== $weight->getUser()->getId()) {
            throw new HttpException(402, "Permission denied");
        }
        try {
            $this->em->remove($weight);
            $this->em->flush();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param WeightDTO $dto
     * @param int $id
     * @param User $user
     * @return BiometricWeight
     * @throws \Exception
     * @throws HttpException
     */
    public function update(WeightDTO $dto, int $id, User $user): BiometricWeight
    {
        $weight = $this->repository->find($id);

        if (!isset($weight)) {
            throw new HttpException(404, "Entity not found");
        }

        if ($user->getId() !== $weight->getUser()->getId()) {
            throw new HttpException(401, "Permission denied");
        }

        $weight->setUpdatedAt(new \DateTime());
        $weight->setCreatedAt($dto->getCreatedAt());
        $weight->setKilograms($dto->getKilograms());
        $weight->setNotes($dto->getNotes());

        try {
            $this->em->persist($weight);
            $this->em->flush();
            return $weight;
        } catch (\Exception $ex) {
            throw new HttpException(500, "Database error");
        }
    }
}