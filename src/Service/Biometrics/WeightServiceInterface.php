<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 09/02/2019
 * Time: 10:40
 */

namespace  App\Service\Biometrics;

use App\Entity\User;
use App\Repository\BiometricWeightRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\BiometricWeight;
use App\DTO\Biometrics\WeightDTO;
use Symfony\Component\HttpKernel\Exception\HttpException;

interface WeightServiceInterface {

    /**
     * WeightServiceInterface constructor.
     * @param EntityManagerInterface $em
     * @param BiometricWeightRepository $repository
     */
    public function __construct(EntityManagerInterface $em, BiometricWeightRepository $repository);

    /**
     *  Read DTO and return and map to entity
     * @param WeightDTO $dto
     * @return BiometricWeight
     */
    public function readDTO(WeightDTO $dto) : BiometricWeight;

    /**
     * Create entry
     * @param WeightDTO $dto
     * @param User $user
     * @return BiometricWeight
     */
    public function create(WeightDTO $dto, User $user) : BiometricWeight;

    /**
     * Update an entry
     * @param WeightDTO $dto
     * @param int $id
     * @param User $user
     * @throws HttpException
     * @return BiometricWeight
     */
    public function update(WeightDTO $dto, int $id, User $user) : BiometricWeight;

    /**
     * Delete an entry
     *
     * @param int $id
     * @param User $user
     * @return bool
     */
    public function delete(int $id, User $user) : bool;


    /** Get counts of days where there was entries
     * @param \DateTime $fromDate
     * @param \DateTime $toDate
     * @param int $userId
     * @param string $sort
     * @return mixed
     */
    public function calendarDateAggregation(\DateTime $fromDate, \DateTime $toDate, int $userId, string $sort);

}