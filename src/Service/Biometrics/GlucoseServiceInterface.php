<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 09/02/2019
 * Time: 10:40
 */

namespace  App\Service\Biometrics;

use App\DTO\Biometrics\GlucoseDTO;
use App\Entity\BiometricGlucose;
use App\Entity\User;
use App\Repository\BiometricGlucoseRepository;
use App\Repository\BiometricWeightRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\BiometricWeight;
use App\DTO\Biometrics\WeightDTO;
use Symfony\Component\HttpKernel\Exception\HttpException;


interface GlucoseServiceInterface {

    /**
     * GlucoseServiceInterface constructor.
     * @param EntityManagerInterface $em
     * @param BiometricGlucoseRepository $repository
     */
    public function __construct(EntityManagerInterface $em, BiometricGlucoseRepository $repository);

    /**
     *  Read DTO and return and map to entity
     * @param GlucoseDTO $dto
     * @return BiometricGlucose
     */
    public function readDTO(GlucoseDTO $dto) : BiometricGlucose;

    /**
     * Create entry
     * @param GlucoseDTO $dto
     * @param User $user
     * @return BiometricGlucose
     */
    public function create(GlucoseDTO $dto, User $user) : BiometricGlucose;

    /**
     * Update an entry
     * @param GlucoseDTO $dto
     * @param int $id
     * @param User $user
     * @throws HttpException
     * @return BiometricGlucose
     */
    public function update(GlucoseDTO $dto, int $id, User $user) : BiometricGlucose;

    /**
     * Delete an entry
     *
     * @param int $id
     * @param User $user
     * @return bool
     */
    public function delete(int $id, User $user) : bool;


    /** Get counts of days where there was entries
     * @param \DateTime $fromDate
     * @param \DateTime $toDate
     * @param int $userId
     * @param string $sort
     * @return mixed
     */
    public function calendarDateAggregation(\DateTime $fromDate, \DateTime $toDate, int $userId, string $sort);

}