<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 09/02/2019
 * Time: 12:05
 */

namespace App\Service;

use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use App\DTO\Security\CredentialLoginDTO;

interface AuthenticationServiceInterface
{
    /**
     * AuthenticationServiceInterface constructor.
     * @param JWTTokenManagerInterface $JWTTokenManager
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(
        JWTTokenManagerInterface $JWTTokenManager,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder);

    public function credentialLogin(CredentialLoginDTO $credentialLoginDTO);
}