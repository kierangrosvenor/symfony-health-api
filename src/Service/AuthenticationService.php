<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 09/02/2019
 * Time: 12:04
 */

namespace App\Service;

use App\DTO\Security\CredentialLoginDTO;
use App\Repository\UserRepository;
use App\View\UserToken;
use Doctrine\ORM\NonUniqueResultException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


final class AuthenticationService implements AuthenticationServiceInterface
{

    /**
     * @var JWTTokenManagerInterface
     */
    private $JWTTokenManager;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * AuthenticationService constructor.
     * @param JWTTokenManagerInterface $JWTTokenManager
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(
        JWTTokenManagerInterface $JWTTokenManager,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $this->JWTTokenManager = $JWTTokenManager;
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param CredentialLoginDTO $credentialLoginDTO
     * @return UserToken
     * @throws HttpException
     */
    public function credentialLogin(CredentialLoginDTO $credentialLoginDTO)
    {
        try {
            $user = $this->userRepository->findOneByEmail(
                trim($credentialLoginDTO->getEmail())
            );
        } catch (NonUniqueResultException $ex) {
            throw new HttpException(404, "Email not found");
        }

        if (isset($user) && $this->passwordEncoder->
            isPasswordValid(
                $user,
                trim($credentialLoginDTO->getPassword()
                ))) {
            $token = new UserToken();
            $token->setToken($this->JWTTokenManager->create($user));
            return $token;
        } else {
            throw new HttpException(401, "Bad credentials");
        }
    }
}