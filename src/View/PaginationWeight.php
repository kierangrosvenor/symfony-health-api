<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 10/02/2019
 * Time: 21:31
 */

namespace App\View;

use Swagger\Annotations as SWG;
use JMS\Serializer\Annotation AS JMS;
use Nelmio\ApiDocBundle\Annotation\Model;

class PaginationWeight {


    /**
     * @JMS\Type("ArrayCollection<App\Entity\BiometricWeight>")
     */
    public $data;


    /**
     * @JMS\Type("ArrayCollection<App\View\CalendarCount>")
     */
    public $calendar;

    /**
     * @JMS\Type("App\View\PaginationInfo")
     */
    private $paginationInfo;

}

