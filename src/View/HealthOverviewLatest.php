<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 16/02/2019
 * Time: 19:29
 */

namespace App\View;

use JMS\Serializer\Annotation AS JMS;

class HealthOverviewLatest {


    /**
     * @JMS\Type("ArrayCollection<App\Entity\BiometricWeight>")
     */
    private $latestWeight;

    /**
     * @JMS\Type("ArrayCollection<App\Entity\BiometricGlucose>")
     */
    private $latestGlucose;


}