<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 10/02/2019
 * Time: 22:38
 */

namespace App\View;

use JMS\Serializer\Annotation AS JMS;

class CalendarCount {

    /**
     * @JMS\Type("datetime")
     */
    public $date;

    /**
     * @JMS\Type("integer")
     */
    public $count;
}