<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 09/02/2019
 * Time: 13:21
 */

namespace App\View;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation AS JMS;

class UserToken
{

    /**
     * @JMS\Type("string")
     */
    private $token;

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token)
    {
        $this->token = $token;
    }
}