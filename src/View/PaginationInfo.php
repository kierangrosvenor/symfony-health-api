<?php
/**
 * Created by PhpStorm.
 * User: kieran
 * Date: 10/02/2019
 * Time: 21:33
 */

namespace App\View;

use JMS\Serializer\Annotation AS JMS;

class PaginationInfo {
    /**
     * @JMS\Type("integer")
     */
    private $count;

    /**
     * @JMS\Type("integer")
     */
    private $prev;


    /**
     * @JMS\Type("integer")
     */
    private $next;

    /**
     * @JMS\Type("integer")
     */
    private $pages;
}
