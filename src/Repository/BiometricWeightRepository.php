<?php

namespace App\Repository;

use App\Entity\BiometricWeight;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BiometricWeight|null find($id, $lockMode = null, $lockVersion = null)
 * @method BiometricWeight|null findOneBy(array $criteria, array $orderBy = null)
 * @method BiometricWeight[]    findAll()
 * @method BiometricWeight[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BiometricWeightRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BiometricWeight::class);
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function queryBuilder(): \Doctrine\ORM\QueryBuilder
    {
        return $this->createQueryBuilder('q');
    }

    //get latest reading added
    public function getLatest(int $userId)
    {
        return $this->createQueryBuilder('q')
            ->where('q.user = :userId')
            ->orderBy('q.createdAt', 'DESC')
            ->getQuery()
            ->setParameter('userId', $userId)
            ->setMaxResults(1)
            ->execute();
    }

    /**
     * @param \DateTime $fromDate
     * @param \DateTime $toDate
     * @param int $userId
     * @param string $sort
     * @return mixed
     */
    public function calendarDateAggregation(\DateTime $fromDate, \DateTime $toDate, int $userId, string $sort)
    {
        return $this->createQueryBuilder('q')
            ->select('
                (YEAR(q.createdAt)) as HIDDEN year, 
                (MONTH(q.createdAt)) AS HIDDEN month,
                (DAY(q.createdAt)) AS HIDDEN day,
                q.createdAt,
                COUNT(q) as count'
            )
            ->where('q.user = :user')
            ->andWhere('q.createdAt >= :fromDate')
            ->andWhere('q.createdAt <= :toDate')
            ->orderBy('q.createdAt', $sort)
            ->groupBy('year')
            ->groupBy('month')
            ->addGroupBy('day')
            ->setParameter('user', $userId)
            ->setParameter('fromDate', $fromDate)
            ->setParameter(':toDate', $toDate)
            ->getQuery()
            ->getResult();
    }


}
