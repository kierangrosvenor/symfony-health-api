<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmailActivateController extends AbstractController
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(
        UserRepository $userRepository,
        EntityManagerInterface $em
    )
    {
        $this->userRepository = $userRepository;
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @Route("/email/activate", name="email_activate")
     * @return  Response
     */
    public function actionActivateEmail(Request $request)
    {

        $user = $this->userRepository->findOneBy(['activationToken' => $request->get('at')]);
        $activated = false;

        if ($user) {
            $user->setIsEmailActivated(true);
            $user->setActivationToken("");
            $this->em->persist($user);
            $this->em->flush();
            $activated = true;
        }

        return $this->render('email_activate/index.html.twig', [
            'controller_name' => 'EmailActivateController',
            'token' => $request->get('at'),
            'user' => $user,
            'activated' => $activated
        ]);
    }
}
