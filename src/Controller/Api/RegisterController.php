<?php

namespace App\Controller\Api;

use App\DTO\RegisterDTO;
use App\DTO\Security\CredentialLoginDTO;
use App\Entity\User;
use App\Producer\EmailActivationProducer;
use App\Service\AuthenticationService;
use Doctrine\ORM\EntityManagerInterface;
use DoctrineExtensions\Query\Mysql\Date;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\View as ViewAnnotation;

use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;


    //private $emailActivationProducer;

    /**
     * RegisterController constructor.
     * @param EntityManagerInterface $em
     * @param AuthenticationService $authenticationService
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param \Swift_Mailer $mailer
     */
    public function __construct(
        EntityManagerInterface $em,
        AuthenticationService $authenticationService,
        UserPasswordEncoderInterface $passwordEncoder,
        \Swift_Mailer $mailer
        //EmailActivationProducer $emailActivationProducer
    )
    {
        $this->em = $em;
        $this->authenticationService = $authenticationService;
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        //$this->emailActivationProducer = $emailActivationProducer;
    }

    /**
     *
     * @ViewAnnotation()
     * @Post("/api/register/")
     * @ParamConverter("registerPostDTO", converter="fos_rest.request_body")
     * @param RegisterDTO $registerPostDTO
     * @param ConstraintViolationListInterface $validationErrors
     * @SWG\Tag(name="user.registration")
     * @SWG\Parameter(
     *     name="user",
     *     in="body",
     *     description="Send RegisterDTO to register a new base user",
     *     @Model(type=App\DTO\RegisterDTO::class)
     * )
     * @SWG\Response(
     *     response=201,
     *     description="User subscribed to the service"
     * )
     *
     * @throws \Exception
     * @return View
     */
    public function postAction(
        RegisterDTO $registerPostDTO,
        ConstraintViolationListInterface $validationErrors)
    {

        if (count($validationErrors) > 0) {
            return View::create($validationErrors, Response::HTTP_BAD_REQUEST);
        }

        $user = new User();
        $user->setCreatedAt(new \DateTime());
        $user->setFirstName(ucfirst(trim($registerPostDTO->getFirstName())));
        $user->setLastName(ucfirst(trim($registerPostDTO->getLastName())));
        $user->setEmail(trim($registerPostDTO->getEmail()));
        $user->setGender($registerPostDTO->getGender());
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                trim($registerPostDTO->getPassword())
            )
        );

        $activationToken = $this->generateActivationToken();
        $user->setActivationToken($activationToken);

        $this->em->persist($user);
        $this->em->flush();

        //send activation email
        $message = new \Swift_Message();
        $message->setSubject('Welcome to HealthApp, please confirm your email address')
            ->setFrom("postmaster@" . getenv('MAILGUN_DOMAIN'))
            ->setTo($user->getEmail())
            ->setBody(
                $this->render("emails/email_verification.html.twig", [
                    'first_name' => $user->getFirstName(),
                    'activationUrl' => $this->generateUrl("app.activate_email", [
                        'at' => $activationToken
                    ], UrlGeneratorInterface::ABSOLUTE_URL)
                ]),
                'text/html'
            );

        $this->mailer->send($message);
        $userCredentials = new CredentialLoginDTO(
            $user->getUsername(),
            $registerPostDTO->getPassword()
        );

        $token = $this->authenticationService->credentialLogin($userCredentials);
        return View::create($token, Response::HTTP_CREATED);
    }

    /**
     * For email activation
     * @return string
     */
    private function generateActivationToken(): string
    {
        $strong = true;
        $bytes = openssl_random_pseudo_bytes(
            64,
            $strong
        );
        return bin2hex($bytes);
    }
}