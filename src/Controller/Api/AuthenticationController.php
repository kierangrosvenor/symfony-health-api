<?php

namespace App\Controller\Api;

use App\DTO\Security\CredentialLoginDTO;
use App\Entity\User;
use App\Service\AuthenticationServiceInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\View as ViewAnnotation;

use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class AuthenticationController extends AbstractController
{

    /**
     * @var AuthenticationServiceInterface
     */
    private $authenticationService;

    public function __construct(AuthenticationServiceInterface $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @ViewAnnotation()
     * @Post("/api/auth/token/")
     * @ParamConverter("credentialLoginDTO", converter="fos_rest.request_body")
     * @param CredentialLoginDTO $credentialLoginDTO
     * @param ConstraintViolationListInterface $validationErrors
     * @SWG\Tag(name="user.authentication")
     * @SWG\Parameter(
     *     name="user",
     *     in="body",
     *     description="Send LoginDTO to register a new base user",
     *     @Model(type=App\DTO\Security\CredentialLoginDTO::class)
     * )
     * @SWG\Response(
     *     response=201,
     *     description="User logged in",
     *     @Model(type=App\View\UserToken::class)
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Email/user not found"
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Incorrect credentials"
     * )
     * @return View
     */
    public function postAction(
        CredentialLoginDTO $credentialLoginDTO,
        ConstraintViolationListInterface $validationErrors) {
        if (count($validationErrors) > 0) {
            return View::create($validationErrors, Response::HTTP_BAD_REQUEST);
        }
        try {
            $token = $this->authenticationService->credentialLogin($credentialLoginDTO);
            return View::create($token, Response::HTTP_OK);
        } catch (HttpException $ex) {
            return View::create($ex->getMessage(), $ex->getStatusCode());
        }
    }
}
