<?php

namespace App\Controller\Api;

use App\DTO\Biometrics\GlucoseDTO;
use App\Entity\User;
use App\Repository\BiometricGlucoseRepository;
use App\Repository\BiometricWeightRepository;
use App\Service\Biometrics\GlucoseServiceInterface;
use App\Service\PaginatorServiceInterface;
use App\View\HealthOverviewLatest;
use Doctrine\ORM\EntityManagerInterface;

use Doctrine\ORM\EntityNotFoundException;
use Knp\Component\Pager\PaginatorInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\View as ViewAnnotation;

use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Delete;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;
use Pagerfanta\Pagerfanta;


class HealthOverviewController extends AbstractController
{

    /**
     * @var BiometricWeightRepository
     */
    private $weightRepository;

    /**
     * @var BiometricGlucoseRepository
     */
    private $glucoseRepository;

    /**
     * @var PaginatorServiceInterface
     */
    private $paginatorService;

    /**
     * @var GlucoseServiceInterface
     */
    private $biometricGlucoseService;

    /**
     * HealthOverviewController constructor.
     * @param BiometricGlucoseRepository $glucoseRepository
     * @param BiometricWeightRepository $weightRepository
     * @param GlucoseServiceInterface $biometricGlucoseService
     */
    public function __construct(
        BiometricGlucoseRepository $glucoseRepository,
        BiometricWeightRepository $weightRepository,
        GlucoseServiceInterface $biometricGlucoseService
    )
    {
        $this->biometricGlucoseService = $biometricGlucoseService;
        $this->weightRepository = $weightRepository;
        $this->glucoseRepository = $glucoseRepository;
    }


    /**
     * @Get("/api/biometrics/latest/")
     * @param Request $request
     * @return View
     * @Security(name="Bearer")
     * @SWG\Tag(name="biometrics.latest")
     * @SWG\Response(
     *     response=200,
     *     description="Success",
     *     @Model(type=App\View\HealthOverviewLatest::class)
     * )
     */
    public function actionLatestHealth(Request $request)
    {



        $data = [

            'latest_glucose' => $this->glucoseRepository
                ->getLatest($this->getUser()->getId()),

            'latest_weight' => $this->weightRepository
                ->getLatest($this->getUser()->getId())

        ];

        return View::create($data, Response::HTTP_OK);
    }

}
