<?php

namespace App\Controller\Api;

use App\DTO\UnitsDTO;
use App\Entity\User;
use App\Producer\EmailActivationProducer;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\View as ViewAnnotation;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;


class UserController extends AbstractController
{
    private $em;
    private $passwordEncoder;

    public function __construct(
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }


    /**
     * @ViewAnnotation()
     * @Put("/api/user/units/")
     * @SWG\Tag(name="user")
     * @param UnitsDTO $unitsDTO
     * @param ConstraintViolationListInterface $validationErrors
     * @ParamConverter("unitsDTO", converter="fos_rest.request_body")
     * @SWG\Parameter(
     *     name="user",
     *     in="body",
     *     description="Send RegisterDTO to register a new base user",
     *     @Model(type=App\DTO\UnitsDTO::class)
     * )
     * @SWG\Response(
     *  response=200,
     *  description="User model",
     *  @Model(type=App\Entity\User::class)
     * )
     * @Security(name="Bearer")
     * @return View
     */
    public function actionPutUnits(UnitsDTO $unitsDTO, ConstraintViolationListInterface $validationErrors)
    {
        if(count($validationErrors) > 0) {
            return View::create($validationErrors, Response::HTTP_BAD_REQUEST);
        }
        $user = $this->getUser();
        if ($user instanceof User) {
           $user->setWeightUnit($unitsDTO->getWeightUnit());
           $user->setGlucoseUnit($unitsDTO->getGlucoseUnit());

           $this->em->persist($user);
           $this->em->flush();

           return View::create($user);
        }
    }


    /**
     * @ViewAnnotation()
     * @Get("/api/user/")
     * @SWG\Tag(name="user")
     * @SWG\Response(
     *  response=200,
     *  description="User model",
     *  @Model(type=App\Entity\User::class)
     * )
     * @Security(name="Bearer")
     * @return View
     */
    public function actionGetUser()
    {
        return View::create($this->getUser(), Response::HTTP_OK);
    }
}
