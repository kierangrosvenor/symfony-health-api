<?php

namespace App\Controller\Api;

use App\DTO\Biometrics\WeightDTO;
use App\Entity\BiometricWeight;
use App\Entity\User;
use App\Repository\BiometricWeightRepository;
use App\Service\PaginatorServiceInterface;
use Doctrine\ORM\EntityManagerInterface;

use Doctrine\ORM\EntityNotFoundException;
use Knp\Component\Pager\PaginatorInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\View as ViewAnnotation;

use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Delete;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;

use Pagerfanta\Pagerfanta;

use App\Service\Biometrics\WeightServiceInterface;

class BiometricWeightController extends AbstractController
{

    /**
     * @var WeightServiceInterface
     */
    private $biometricWeightService;

    /**
     * @var BiometricWeightRepository
     */
    private $repository;

    /**
     * @var PaginatorServiceInterface
     */
    private $paginatorService;

    /**
     * BiometricWeightController constructor.
     * @param WeightServiceInterface $biometricWeightService
     * @param PaginatorServiceInterface $paginatorService
     * @param BiometricWeightRepository $repository
     */
    public function __construct(
        WeightServiceInterface $biometricWeightService,
        PaginatorServiceInterface $paginatorService,
        BiometricWeightRepository $repository)
    {

        $this->biometricWeightService = $biometricWeightService;
        $this->paginatorService = $paginatorService;
        $this->repository = $repository;
    }

    /**
     * @Post("/api/biometric/weight/")
     * @ParamConverter("weightDTO", converter="fos_rest.request_body")
     * @param WeightDTO $weightDTO
     * @param ConstraintViolationListInterface $validationErrors
     * @return View
     * @Security(name="Bearer")
     * @SWG\Tag(name="biometrics.weight")
     * @SWG\Parameter(
     *     name="weightDTO",
     *     in="body",
     *     description="Add weight",
     *     @Model(type=App\DTO\Biometrics\WeightDTO::class)
     * )
     * @SWG\Response(
     *     response=201,
     *     description="Success",
     *     @Model(type=App\Entity\BiometricWeight::class)
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     * )
     */
    public function postWeightAction(
        WeightDTO $weightDTO,
        ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            return View::create($validationErrors, Response::HTTP_BAD_REQUEST);
        }
        try {
            $weight = $this->biometricWeightService->create(
                $weightDTO,
                $this->getUser()
            );
            return View::create($weight, Response::HTTP_CREATED);
        } catch (\Exception $ex) {
            return View::create($ex, Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @Put("/api/biometric/weight/{id}")
     * @ParamConverter("weightDTO", converter="fos_rest.request_body")
     * @param WeightDTO $weightDTO
     * @param ConstraintViolationListInterface $validationErrors
     * @param int $id
     * @return View
     * @Security(name="Bearer")
     * @SWG\Tag(name="biometrics.weight")
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="Identifier",
     * )
     * @SWG\Parameter(
     *     name="weightDTO",
     *     in="body",
     *     description="Update weight",
     *     @Model(type=App\DTO\Biometrics\WeightDTO::class)
     * )
     * @SWG\Response(
     *     response=201,
     *     description="Success",
     *     @Model(type=App\Entity\BiometricWeight::class)
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     * )
     */
    public function putWeightAction(
        int $id,
        WeightDTO $weightDTO,
        ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            return View::create($validationErrors, Response::HTTP_BAD_REQUEST);
        }
        try {
            $weight = $this->biometricWeightService->update(
                $weightDTO,
                $id,
                $this->getUser()
            );
            return View::create($weight, Response::HTTP_CREATED);
        } catch (HttpException $ex) {
            return View::create($ex->getMessage(), $ex->getStatusCode());
        }
    }

    /**
     * @Delete("/api/biometric/weight/{id}")
     * @param int $id
     * @return View
     * @Security(name="Bearer")
     * @SWG\Tag(name="biometrics.weight")
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="Identifier"
     * )
     * @SWG\Response(
     *     response=201,
     *     description="Weight reading removed"
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Bad request"
     * )
     */
    public function deleteWeightAction(int $id)
    {
        try {
            $done = $this->biometricWeightService->delete($id, $this->getUser());
            if ($done) {
                return View::create("Removed", Response::HTTP_OK);
            }

        } catch (HttpException $ex) {
            return View::create($ex->getMessage(), $ex->getStatusCode());
        }
    }


    /**
     * @Get("/api/biometric/weight/list")
     * @param Request $request
     * @return View
     * @Security(name="Bearer")
     * @SWG\Tag(name="biometrics.weight")
     * @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     default=1,
     *     type="integer",
     *     required=true,
     *     description="Page",
     * )
     * @SWG\Parameter(
     *     name="per_page",
     *     in="query",
     *     default=25,
     *     type="integer",
     *     required=true,
     *     description="Per page",
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     required=true,
     *     type="string",
     *     enum={"ASC", "DESC"},
     *     description="Sort by created date ASC or DESC"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Success",
     *     @Model(type=App\View\PaginationWeight::class)
     * )
     **/
    public function actionWeightList(Request $request)
    {

        $page = $request->get('page') ? $request->get('page') : 1;
        $perPage = $request->get('per_page') ? $request->get('per_page') : 25;
        $sort = $request->get('sort') ? $request->get('sort') : 'ASC';

        $qb = $this->repository->queryBuilder()
            ->andWhere('q.user = :userId')
            ->orderBy('q.createdAt', $sort)
            ->setParameter('userId', $this->getUser()->getId());

        $data = $this->paginatorService->paginate($qb, $page, $perPage, $sort);

        return View::create($data, Response::HTTP_OK);
    }


    /**
     * @Get("/api/biometric/weight/between")
     * @param Request $request
     * @return View
     * @Security(name="Bearer")
     * @SWG\Tag(name="biometrics.weight")
     * @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     default=1,
     *     type="integer",
     *     required=true,
     *     description="Page",
     * )
     * @SWG\Parameter(
     *     name="per_page",
     *     in="query",
     *     default=25,
     *     type="integer",
     *     required=true,
     *     description="Per page",
     * )
     * @SWG\Parameter(
     *     name="from_date",
     *     in="query",
     *     type="string",
     *     required=true,
     *     format="date-time",
     *     default="2019-02-01",
     *     description="ISO 8601 formatted date",
     * )
     * @SWG\Parameter(
     *     name="to_date",
     *     in="query",
     *     required=true,
     *     type="string",
     *     format="date-time",
     *     default="2019-02-10",
     *     description="ISO 8601 formatted date"
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     required=true,
     *     type="string",
     *     enum={"ASC", "DESC"},
     *     description="Sort by created date ASC or DESC"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Success",
     *     @Model(type=App\View\PaginationWeight::class)
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     * )
     */
    public function actionListBetween(Request $request)
    {

        try {
            $page = $request->get('page') ? $request->get('page') : 1;
            $perPage = $request->get('per_page') ? $request->get('per_page') : 25;
            $fromDate = new \DateTime($request->get('from_date'));
            $toDate = new \DateTime($request->get('to_date'));
            $sort = $request->get('sort') ? $request->get('sort') : 'ASC';

            $fromDate->setTime(0, 0, 0);
            $toDate->setTime(23, 59, 59);

            $qb = $this->repository->queryBuilder()
                ->where('q.createdAt >= :fromDate')
                ->andWhere('q.createdAt <= :toDate')
                ->andWhere('q.user = :userId')
                ->orderBy('q.createdAt', $sort)
                ->setParameter('fromDate', $fromDate->format('Y-m-d H:i:s'))
                ->setParameter('toDate', $toDate->format('Y-m-d H:i:s'))
                ->setParameter('userId', $this->getUser()->getId());

            $data = $this->paginatorService->paginate($qb, $page, $perPage, $sort);
            $dates = $this->biometricWeightService
                ->calendarDateAggregation(
                    $fromDate,
                    $toDate,
                    $this->getUser()->getId(),
                    $sort
                );
            $calendar = [
                'calendar' => $dates
            ];
            $data = array_merge($calendar, $data);

            return View::create($data, Response::HTTP_OK);
        } catch (\Exception $ex) {
            return View::create($ex->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}
